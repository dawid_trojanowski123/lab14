package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class UserIdSpec extends Specification {

    @Unroll
    def "should check pesel is correct for #id"() {
        when:
            def userId = new UserId(id)
        then:
            userId.isCorrect() == result
        where:
            id                  | result
            ''                  | false
            '432123'            | false
            '64102532924'       | true
            'asdafwe'           | false
            '2342342342342'     | false
    }

    @Unroll
    def "should check pesel size for #id"() {
        when:
            def userId = new UserId(id)
        then:
            userId.isCorrectSize() == result
        where:
            id              | result
            ''              | false
            '4324'          | false
            '64102532924'   | true
    }

    @Unroll
    def "should check date for #id"() {
        when:
            def userId = new UserId(id)
        then:
            userId.date.orElse(null) == result
        where:
            id              | result
            '64102532924'   | '25-10-1964'
            ''              | null
            'asdasd'        | null
            '231312'        | null
    }

    @Unroll
    def "should check sex for #id"() {
        when:
            def userId = new UserId(id)
        then:
            userId.sex.orElse(null) == result
        where:
            id | result
            '' | null
            '59100537514' | UserIdChecker.Sex.MAN
            '78022667884' | UserIdChecker.Sex.WOMAN
    }
}
