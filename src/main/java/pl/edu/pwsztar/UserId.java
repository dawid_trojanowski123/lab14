package pl.edu.pwsztar;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

final class UserId implements UserIdChecker {

    private final String id;

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
        return id.length() == 11;
    }

    @Override
    public Optional<Sex> getSex() {
        if (!isPeselValid()) {
            return Optional.empty();
        }
        byte[] peselBytes = getPeselBytes();
        if (peselBytes[9] % 2 == 1) {
            return Optional.ofNullable(Sex.MAN);
        } else {
            return Optional.ofNullable(Sex.WOMAN);
        }
    }

    @Override
    public boolean isCorrect() {
        return isPeselValid();
    }

    @Override
    public Optional<String> getDate() {
        if (!isPeselValid()) {
            return Optional.empty();
        }

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate localDate = LocalDate.of(getBirthYear(), getBirthMonth(), getBirthDay());

        return Optional.of(localDate.format(dateTimeFormatter));
    }

    private boolean isPeselValid() {
        if (!isCorrectSize()){
            return false;
        } else {
            byte[] peselBytes = new byte[11];
            for (int i = 0; i < 11; i++){
                peselBytes[i] = Byte.parseByte(id.substring(i, i+1));
            }
            return checkSum() && checkMonth() && checkDay();
        }
    }

    private int getBirthYear() {
        byte[] peselBytes = getPeselBytes();
        int year;
        int month;
        year = 10 * peselBytes[0];
        year += peselBytes[1];
        month = 10 * peselBytes[2];
        month += peselBytes[3];
        if (month > 80 && month < 93) {
            year += 1800;
        }
        else if (month > 0 && month < 13) {
            year += 1900;
        }
        else if (month > 20 && month < 33) {
            year += 2000;
        }
        else if (month > 40 && month < 53) {
            year += 2100;
        }
        else if (month > 60 && month < 73) {
            year += 2200;
        }
        return year;
    }

    private int getBirthMonth() {
        byte[] peselBytes = getPeselBytes();
        int month;
        month = 10 * peselBytes[2];
        month += peselBytes[3];
        if (month > 80 && month < 93) {
            month -= 80;
        }
        else if (month > 20 && month < 33) {
            month -= 20;
        }
        else if (month > 40 && month < 53) {
            month -= 40;
        }
        else if (month > 60 && month < 73) {
            month -= 60;
        }
        return month;
    }

    private byte[] getPeselBytes() {
        byte[] peselBytes = new byte[11];
        for (int i = 0; i < 11; i++){
            peselBytes[i] = Byte.parseByte(id.substring(i, i+1));
        }

        return peselBytes;
    }

    private int getBirthDay() {
        byte[] peselBytes = getPeselBytes();
        int day;
        day = 10 * peselBytes[4];
        day += peselBytes[5];
        return day;
    }

    private boolean checkSum() {
        byte[] peselBytes = getPeselBytes();
        int sum = 1 * peselBytes[0] +
                3 * peselBytes[1] +
                7 * peselBytes[2] +
                9 * peselBytes[3] +
                1 * peselBytes[4] +
                3 * peselBytes[5] +
                7 * peselBytes[6] +
                9 * peselBytes[7] +
                1 * peselBytes[8] +
                3 * peselBytes[9];
        sum %= 10;
        sum = 10 - sum;
        sum %= 10;

        if (sum == peselBytes[10]) {
            return true;
        }
        else {
            return false;
        }
    }

    private boolean checkMonth() {
        int month = getBirthMonth();
        int day = getBirthDay();
        if (month > 0 && month < 13) {
            return true;
        }
        else {
            return false;
        }
    }

    private boolean checkDay() {
        int year = getBirthYear();
        int month = getBirthMonth();
        int day = getBirthDay();
        if ((day >0 && day < 32) &&
                (month == 1 || month == 3 || month == 5 ||
                        month == 7 || month == 8 || month == 10 ||
                        month == 12)) {
            return true;
        }
        else if ((day >0 && day < 31) &&
                (month == 4 || month == 6 || month == 9 ||
                        month == 11)) {
            return true;
        }
        else if ((day >0 && day < 30 && leapYear(year)) ||
                (day >0 && day < 29 && !leapYear(year))) {
            return true;
        }
        else {
            return false;
        }
    }

    private boolean leapYear(int year) {
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
            return true;
        else
            return false;
    }
}
